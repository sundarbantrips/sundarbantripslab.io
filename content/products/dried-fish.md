---
title: "Dried Fish"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Hygenic Dried Fish"

# product Price
price: "250"
# priceBefore: "49.00"

amount: "250gm"
# Product Short Description
shortDescription: "Hygenic Dried Fish"

#product ID
productID: "6"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/dried-fish.webp"

---
