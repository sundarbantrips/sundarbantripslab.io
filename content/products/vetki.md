---
title: "Packaged Solid Fish Vetki"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Packaged Solid Fish Vetki"

# product Price
price: "700"
# priceBefore: "49.00"

amount: "1 KG"
# Product Short Description
shortDescription: "Packaged Solid Fish Vetki"

#product ID
productID: "13"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/vetki.webp"

---
