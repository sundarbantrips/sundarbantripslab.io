---
title: "Sieve"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Sieve"

# product Price
price: "150"
# priceBefore: "49.00"

amount: "1 Piece"
# Product Short Description
shortDescription: "Sieve"

#product ID
productID: "28"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/sieve.webp"

---
