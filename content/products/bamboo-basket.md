---
title: "Babmboo Basket"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Handmade Babmboo Basket"

# product Price
price: "150"


# product unit
amount: "1 piece"
# Product Short Description
shortDescription: "Handmade Babmboo Basket"

#product ID
productID: "1"

# type must be "products"
type: "products"
# product Images
# first image will be shown in the product page
images:
  - image: "images/products/basket.webp"
  # - image: "images/products/product-1.webp"
  # - image: "images/products/product-3.webp"
  # - image: "images/products/product-4.webp"
---
