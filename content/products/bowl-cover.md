---
title: "Bowl Cover"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Bowl Cover"

# product Price
price: "150"
# priceBefore: "49.00"

amount: "1 Piece"
# Product Short Description
shortDescription: "Bowl Cover"

#product ID
productID: "24"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/bowl-cover.webp"

---
