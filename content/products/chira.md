---
title: "Flattened rice (Chira)"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Flattened rice (Chira)"

# product Price
price: "70"
# priceBefore: "49.00"

amount: "500 gm"
# Product Short Description
shortDescription: "Flattened rice (Chira)"

#product ID
productID: "4"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/chira.webp"

---
