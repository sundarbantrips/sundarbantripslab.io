---
title: "Handmade Fan"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Handmade Fan"

# product Price
price: "200"
# priceBefore: "49.00"

amount: "1 Piece"
# Product Short Description
shortDescription: "Handmade Fan"

#product ID
productID: "19"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/fan.webp"

---
