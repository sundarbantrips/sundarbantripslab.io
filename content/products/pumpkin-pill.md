---
title: "Pumpkin Pill"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Pumpkin Pill"

# product Price
price: "200"
# priceBefore: "49.00"

amount: "500 gm"
# Product Short Description
shortDescription: "Pumpkin Pill"

#product ID
productID: "15"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/pumpkin-pill.webp"

---
