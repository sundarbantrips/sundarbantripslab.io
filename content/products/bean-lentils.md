---
title: "Bean Lentils"
date: 2019-10-17T11:22:16+06:00
draft: false


# meta description
description : "Bean Lentils"

# product Price
price: "90"

amount: "500 gm"
# Product Short Description
shortDescription: "Bean Lentils"

#product ID
productID: "2"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/bean-lentils.webp"

---
