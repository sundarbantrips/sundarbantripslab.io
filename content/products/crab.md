---
title: "Packaged Solid Crab"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Packaged Solid Crab"

# product Price
price: "300"
# priceBefore: "49.00"

amount: "500 gm"
# Product Short Description
shortDescription: "Packaged Solid Crab"

#product ID
productID: "5"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/crab.webp"

---
