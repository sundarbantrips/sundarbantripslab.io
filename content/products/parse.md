---
title: "Packaged Solid Fish Parse"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Packaged Solid Fish Parse"

# product Price
price: "250"
# priceBefore: "49.00"

amount: "500 gm"
# Product Short Description
shortDescription: "Packaged Solid Fish Vetki"

#product ID
productID: "10"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/parse.webp"

---
