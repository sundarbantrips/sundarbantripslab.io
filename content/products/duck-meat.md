---
title: "Duck Meat"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Duck Meat"

# product Price
price: "300"

amount: "500 gm"

# Product Short Description
shortDescription: "Duck Meat"

#product ID
productID: "8"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/duck.webp"

---
