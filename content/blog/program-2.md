---
title: "Nature Observation Program of Summer"
date: 2020-12-27T11:22:16+06:00
draft: false
description : "Nature Observation Program of Summer"
image: "images/blog/summer-1.webp"
categories:
- Program
---

**At a glance of nature observation program of Summer (March-June)**

- Visit to the Sundarbans

Visitors can easily move to Kolagachiaeco tourism Center by using local transport and boat to enjoy the mangrove ecosystem and learn the biodiversity of the Sundarbans

- Visiting Akashlina Eco Tourism Center

Visitors can easily move to the Akashlina Eco Tourism Center from the Cyclone Shelter using local transportation vehicles to enjoy the scenic beauty of the center and learn about the mangroves

- Observe the Mangroves

Visitors can observe mangroves deeply staying with the Nature Observation team (pneumatophore, mangrove leaves and flower, difference between tall and bushy mangrove etc.).

- Learn about Sundarbans flowers

In summer season different flowers bloom in the Sundarbans. The visitors can learn from the villagers about the period of flower blooming, time of fruit maturation, mangrove saplings creation from seed etc.

- Observe Sundarbans Wildlife

The visitors can learn about the fisheries of the Sundarbans by visiting the local markets and staying with the fishermen. They can also catch fishes and crabs with the local fishermen and also cook and enjoy those fish and crab with the fishermen.

- Observe and catch Sundarbans fish and crab

The visitors can learn about the fisheries of the Sundarbans by visiting the local markets and staying with the fishermen. They can also catch fishes and crabs with the local fishermen and also cook and enjoy those fish and crab with the fishermen.

- Honey Collection

The tourists can observe the method of honey collection by the honey collectors

- Boat riding

There is a small canal passes by the Cyclone Shelter. Visitors can ride on boat in this river or small canals with the help of the local fishermen.

- Storytelling and experience Sharing

Visitors can learn special stories from the experienced forest dwellers and share their own experience.

- Enjoy the Munda (indigenous) culture

Tourists can enjoy the culture of Munda community with low cost staying inside the Center. They can also visit Munda villages to mitigate their curiosity.
