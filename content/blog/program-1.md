---
title: "Nature Observation program of winter"
date: 2020-12-27T11:22:16+06:00
draft: false
description : "Nature Observation program of winter"
image: "images/blog/winter-1.webp"
categories:
- Program
---

**At a glance of Nature Observation program of winter (November to February)**

- Observe the mangroves

Visitors can easily move to Kolagachiaeco tourism Center by using local transport and boat to enjoy the mangrove ecosystem and learn the biodiversity of the Sundarbans

- Visiting Akashlina Eco Tourism Center

Visitors can easily move to the Akashlina Eco Tourism Center from the Cyclone Shelter using local transportation vehicles to enjoy the scenic beauty of the center and learn about the mangroves.

- Observe the Mangroves

Visitors can observe mangroves deeply staying with the Nature Observation team (pneumatophore, mangrove leaves and flower, difference between tall and bushy mangrove etc.).

- Observe Sundarbans Wildlife

Visitors can observe different wildlife&#39;s on the bank of rivers/canals of Sundarbans like Mud Skipper, Red Crab, Bee etc. and learn about their importance on mangrove ecosystem conservation.

- Bird watching

Observe and catch Sundarbans fish and crab

- The visitors can learn about the fisheries of the Sundarbans by visiting the local markets and staying with the fishermen. They can also catch fishes and crabs with the local fishermen and also cook and enjoy those fish and crab with the fishermen.
- Seed sowing and crop harvesting

The visitors can cultivate and collect winter vegetables with the farmers according to their will. They can cook and enjoy the harvested vegetables with the villagers.

- Sweetmeats and cake of date juice

Visitors can enjoy the traditional date cake and payes (sweet rice) made of date juice according to their will.

- Enjoy the Munda (indigenous) culture

Tourists can enjoy the culture of Munda community with low cost staying inside the Center. They can also visit Munda villages to mitigate their curiosity.

- Storytelling and experience sharing

Visitor can get the opportunity to learn stories of the experienced forest dwellers and can share their experiences with them

- Enjoy the Munda (indigenous) culture

Tourists can enjoy the culture of Munda community with low cost staying inside the Center. They can also visit Munda villages to mitigate their curiosity.
